FROM debian:bookworm-20241111

RUN apt update && apt install -y \
    wget \
    unzip \
    nginx \
    tzdata \
    ca-certificates \
    lsb-release \
    apt-transport-https \
    ca-certificates \
    cron \
    sudo \
    nano \
    less \
    locales \
    curl

# Set locale to de_DE.UTF-8
# set convert-meta enables the input of 8 bit characters like (öäü)
RUN sed -i -e 's/# de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=de_DE.UTF-8
ENV LANG de_DE.UTF-8

RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg && \
    echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list


RUN apt update && apt install -y \
    php8.3 \
    php8.3-fpm \
    php8.3-mysql \
    php8.3-curl \
    php8.3-gd \
    php8.3-xml \
    php8.3-mbstring \
    php8.3-intl \
    php8.3-zip \
    php8.3-redis \
    php8.3-soap \
    php8.3-bcmath \
    php8.3-oauth && \
    rm -rf /var/lib/apt/lists/*

RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && \
    chmod +x wp-cli.phar && \
    mv wp-cli.phar /usr/local/bin/wp

# Install civicrm cli
RUN curl -LsS https://download.civicrm.org/cv/cv.phar -o /usr/local/bin/cv && \
    chmod +x /usr/local/bin/cv

# Install civix
RUN curl -LsS https://download.civicrm.org/civix/civix.phar -o /usr/local/bin/civix && \
    chmod +x /usr/local/bin/civix

WORKDIR /html

RUN mkdir -p /run/php && \
    sed -i 's/error_log = \/var\/log\/php8\.3-fpm\.log/error_log = \/dev\/stderr/g' /etc/php/8.3/fpm/php-fpm.conf && \
    sed -i 's/;daemonize = yes/daemonize = no/g' /etc/php/8.3/fpm/php-fpm.conf && \
    sed -i 's/listen = \/run\/php\/php8\.3-fpm.sock/listen = 9000/g' /etc/php/8.3/fpm/pool.d/www.conf && \
    sed -i 's/;request_terminate_timeout = 0/request_terminate_timeout = 600/g' /etc/php/8.3/fpm/pool.d/www.conf && \
    sed -i 's/;request_terminate_timeout_track_finished = no/request_terminate_timeout_track_finished = yes/g' /etc/php/8.3/fpm/pool.d/www.conf && \
    sed -i 's/;decorate_workers_output = no/decorate_workers_output = no/g' /etc/php/8.3/fpm/pool.d/www.conf && \
    sed -i 's/;catch_workers_output = yes/catch_workers_output = yes/g' /etc/php/8.3/fpm/pool.d/www.conf && \
    sed -i 's/user = nobody/user = www-data/g' /etc/php/8.3/fpm/pool.d/www.conf && \
    sed -i 's/group = nobody/group = www-data/g' /etc/php/8.3/fpm/pool.d/www.conf && \
    sed -i 's/pm.max_children = 5/pm.max_children = 100/g' /etc/php/8.3/fpm/pool.d/www.conf && \
    sed -i 's/pm.start_servers = 2/pm.start_servers = 5/g' /etc/php/8.3/fpm/pool.d/www.conf && \
    sed -i 's/pm.min_spare_servers = 1/pm.min_spare_servers = 5/g' /etc/php/8.3/fpm/pool.d/www.conf && \
    sed -i 's/pm.max_spare_servers = 3/pm.max_spare_servers = 25/g' /etc/php/8.3/fpm/pool.d/www.conf && \
    sed -i 's/user = www-data/user = www-data/g' /etc/php/8.3/fpm/pool.d/www.conf && \
    sed -i 's/group = www-data/group = www-data/g' /etc/php/8.3/fpm/pool.d/www.conf && \
    sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' /etc/php/8.3/fpm/php.ini && \
    sed -i 's/memory_limit = 128M/memory_limit = 512M/g' /etc/php/8.3/fpm/php.ini && \
    sed -i 's/expose_php = On/expose_php = Off/g' /etc/php/8.3/fpm/php.ini && \
    sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 50M/g' /etc/php/8.3/fpm/php.ini && \
    sed -i 's/post_max_size = 8M/post_max_size = 51M/g' /etc/php/8.3/fpm/php.ini && \
    rm /var/log/nginx/access.log /var/log/nginx/error.log && \
    ln -s /dev/null /var/log/nginx/access.log && \
    ln -s /dev/stdout /var/log/nginx/error.log && \
    (crontab -l ; echo "*/5 * * * * sudo -u www-data wp --user=dominik --url=https://suma-ev.de --path=/html/ civicrm api job.execute auth=0") | crontab - && \
    (crontab -l ; echo "*/5 * * * * wget -qO- https://suma-ev.de/wp-cron.php &> /dev/null") | crontab -

CMD cron -L /dev/stdout && \
    php-fpm8.3

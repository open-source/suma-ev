# SUMA-EV

Helm Chart for SUMA-EV Website

## Nasty Civicrm Cookie
Civicrm starts a php session on every page visit. To prevent that you have to modify `wp-content/plugins/civicrm/civicrm.php`. And replace:
```php
if (empty($session_id) && !$wp_cron && !$wp_cli && !$php_cli) {
    session_start();
}
```
with
```php
if (empty($session_id) && !$wp_cron && !$wp_cli && !$php_cli) {
    global $wp;
    if(stripos($_SERVER['REQUEST_URI'], "/wp-admin/") !== FALSE){
        session_start();
    }
}
```